# go 工具集

## ini_parser

解析ini配置文件, 文件格式形如：

```txt
[section1]
    option1 = xxxx
    option2 = xxxx
# start with '#' is a comment
; start with ';' is a comment too
[section2]
    option3 = xxxxx
```

解析出的结果放到IniConfig中，其结构为map[string]Section, 可以通过Get方法获取某个section. 也可以通过GetOption方法获取某个配置项

Section数据结构map[string]string, 可以通过Get来获取某些option配置

## json_parser

根据json文件路径解析json数据

## collection

### Set

Set数据结构
