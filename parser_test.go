package gkit

import (
	"fmt"
	"testing"
)

func TestParse(t *testing.T) {
	ini, err := ParseIni("test.ini")
	if err != nil {
		t.FailNow()
	}
	fmt.Println(ini.GetOption("section1", "option1"))
	fmt.Println(ini.Get("section1").Get("option2"))
}
