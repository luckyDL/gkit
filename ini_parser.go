package gkit

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"

	"container/list"
)

// Section 配置文件中某section配置项
type Section map[string]string

// IniConfig 配置
type IniConfig map[string]Section

// Get 获取配置section
func (c IniConfig) Get(key string) Section {
	value, ok := c[key]
	if ok {
		return value
	}
	// log.Fatalln("Failed to get section by key " + key)
	return nil
}

// GetOption 从config中获取某个配置
func (c IniConfig) GetOption(sKey string, optKey string) string {
	s := c.Get(sKey)
	if s == nil {
		return ""
	}
	return s.Get(optKey)
}

// SetSection 添加配置项
func (c IniConfig) set(key string, section Section) {
	key = strings.Trim(key, " ")
	c[key] = section
}

func (c IniConfig) new() IniConfig {
	return make(map[string]Section)
}

func (s Section) new() Section {
	return make(map[string]string)
}

// SetOption 增加配置项
func (s Section) set(key string, value string) {
	key = strings.Trim(key, " ")
	value = strings.Trim(value, " ")
	s[key] = value
}

// Get 从section获取某个配置值
func (s Section) Get(key string) string {
	value, ok := s[key]
	if ok {
		return value
	}
	// log.Fatalln("Failed to get option by key " + key)
	return ""
}

// ParseIni parse config data from a ini file.
// file like:
// [section1]
//     option1 = xxxx
//     option2 = xxxx
// # start with '#' is a comment
// ; start with ';' is a comment too
// [section2]
//     option3 = xxxxx
//     ...
func ParseIni(path string) (IniConfig, error) {
	lst, err := readFile(path)
	if err != nil {
		return nil, err
	}
	var iniConfig IniConfig
	iniConfig = iniConfig.new()
	var cuSection Section
	var cuSecKey string
	e := lst.Front()
	for {
		line, _ := e.Value.([]byte)
		e = e.Next()
		if isSection(line) {
			if cuSection != nil {
				iniConfig.set(cuSecKey, cuSection)
				cuSection = cuSection.new()
			} else {
				cuSection = cuSection.new()
			}
			cuSecKey = string(line[1 : len(line)-1])
		} else {
			strs := strings.Split(string(line), "=")
			cuSection.set(strs[0], strs[1])
		}
		if e == nil {
			iniConfig.set(cuSecKey, cuSection)
			break
		}
	}
	return iniConfig, nil
}

// 从文件中读取数据
// return 行数据列表
func readFile(path string) (*list.List, error) {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return nil, err
	}
	if fi, _ := file.Stat(); fi.IsDir() || fi.Size() == 0 {
		return nil, fmt.Errorf("Invalid file '%s'", path)
	}
	br := bufio.NewReader(file)
	l := list.New()
	for {
		line, e := br.ReadBytes('\n')
		if e == io.EOF {
			break
		}
		line = line[0 : len(line)-1]
		line = bytes.Trim(bytes.Trim(bytes.TrimRight(line, "\n"), " "), "\t")
		line = bytes.TrimFunc(line, func(r rune) bool { return r == 13 })
		if len(line) == 0 || isComment(line) {
			continue
		}
		l.PushBack(line)
	}
	return l, nil
}

// isComment 是否为注释行
func isComment(line []byte) bool {
	return line[0] == '#' || line[0] == ';'
}

// isSection 判断该行是否是注释
func isSection(line []byte) bool {
	return line[0] == '[' && line[len(line)-1] == ']'
}
