package gkit

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"regexp"
)

// ParseJSON 解析配置文件
func ParseJSON(filePath string, data interface{}) interface{} {
	fmt.Println("parsing file: ", filePath)
	configFile, err := os.Open(filePath)
	if err != nil {
		return data
	}
	fi, _ := configFile.Stat()

	if fi.Size() == 0 {
		return data
	}

	buffer := make([]byte, fi.Size())
	_, err = configFile.Read(buffer)

	buffer, err = stripComments(buffer) //去掉注释
	if err != nil {
		return data
	}
	buffer = []byte(os.ExpandEnv(string(buffer))) //特殊
	err = json.Unmarshal(buffer, &data)           //解析json格式数据
	if err != nil {
		return data
	}
	return data
}

func stripComments(data []byte) ([]byte, error) {
	data = bytes.Replace(data, []byte("\r"), []byte(""), 0) // Windows
	lines := bytes.Split(data, []byte("\n"))                //split to muli lines
	filtered := make([][]byte, 0)

	for _, line := range lines {
		match, err := regexp.Match(`^\s*#`, line)
		if err != nil {
			return nil, err
		}
		if !match {
			filtered = append(filtered, line)
		}
	}
	return bytes.Join(filtered, []byte("\n")), nil
}
