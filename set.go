package gkit

// Set 自定义Set类型
type Set struct {
	m map[interface{}]struct{}
}

// NewSet create an new set
func NewSet() *Set {
	s := &Set{}
	s.m = map[interface{}]struct{}{}
	return s
}

// AddSet  add a set into this one
func (s *Set) AddSet(set Set) {
	for key := range set.m {
		s.Add(key)
	}
}

// Add add an item into this set
func (s *Set) Add(key interface{}) {
	s.m[key] = struct{}{}
}

// IsEmpty whether an empty set
func (s *Set) IsEmpty() bool {
	return len(s.m) < 1 || s == nil
}

// ToString to string
func (s *Set) ToString() string {
	result := "["
	for key := range s.m {
		result = result + key.(string) + ", "
	}
	if len(result) > 1 {
		result = result[0 : len(result)-2]
	}
	result += "]"
	return result
}
